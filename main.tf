// Configure the Google Cloud provider
//terraform google_compute_instance multiple instances
//please execute export GOOGLE_PROJECT=lively-experiment

#terraform {
#  backend "remote" {
#    hostname     = "app.terraform.io"
#    organization = "example-org-5c0a5c"

#    workspaces {
#      name = "getting-started"
#    }
#  }
#}


provider "google" {
 credentials = file("lively-experiment-4d62db9d9624.json")
 project     = "lively-experiment"
 region      = "asia-southeast2"
}

locals {
  names = toset([
    "nosul-5"
  ])
}
//test
// Terraform plugin for creating random ids
#resource "random_id" "instance_id" {
 #byte_length = 8
#}

resource "google_compute_instance_from_machine_image" "tpl" {
  for_each = local.names
  provider = google-beta
  #name     = "terraform-vm-fromimage-${random_id.instance_id.hex}"
  name = each.key
  zone     = "asia-southeast2-a"
  source_machine_image = "projects/lively-experiment/global/machineImages/template-nosul-client"
  // Override fields from machine image
  can_ip_forward = false

  #labels = {
   # my_key = "my_value"
  #}
}
